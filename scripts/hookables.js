'use strict';
class WrapperListener {
    #before;
    #after;

    constructor() {
        this.#before = new WrapperBeforeEvent();
        this.#after = new WrapperAfterEvent();
    }

    get mixedWrapper() {
        const listener = this;
        return async function (wrapped, ...args) {
            args = await listener.#before.fire(this, ...args);
            if (args === false) {
                return false;
            }
            let result = await wrapped(...args);
            result = await listener.#after.fire(this, result);
            return result;
        };
    }

    get before() {
        return this.#before;
    }
    get after() {
        return this.#after;
    }
}

class WrapperEvent {
    constructor() {
        this.callbacks = [];
        this.onceCallbacks = [];
    }
    on(callback) {
        this.callbacks.push(callback);
    }
    one(callback) {
        this.onceCallbacks.push(callback);
    }
}

class WrapperBeforeEvent extends WrapperEvent {
    static async #runCallbacks(callbacks, wrapperThis, ...args) {
        for (const callback of callbacks) {
            const response = await callback(wrapperThis, ...args);
            if (response === false) {
                return false;
            }
            if (response !== undefined) {
                args = response;
            }
        }
        return args;
    }

    async fire(wrapperThis, ...args) {
        args = await WrapperBeforeEvent.#runCallbacks(this.callbacks, wrapperThis, ...args);
        if (args === false) {
            return args;
        }
        args = await WrapperBeforeEvent.#runCallbacks(this.onceCallbacks, wrapperThis, ...args);
        this.onceCallbacks = [];
        return args;
    }
}

class WrapperAfterEvent extends WrapperEvent {
    static async #runCallbacks(callbacks, wrapperThis, result) {
        for (const callback of callbacks) {
            const response = await callback(wrapperThis, result);
            if (response !== undefined) {
                result = response;
            }
        }
        return result
    }

    async fire(wrapperThis, result) {
        result = await WrapperAfterEvent.#runCallbacks(this.callbacks, wrapperThis, result);
        const onceCallbacks = this.onceCallbacks;
        this.onceCallbacks = [];
        result = await WrapperAfterEvent.#runCallbacks(onceCallbacks, wrapperThis, result);
        return result;
    }
}
