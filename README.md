# BeTo - Library

This module does not feature any functionality on itself. It just facilitates some useful functions that are used by multiple different BeTo Modules.

If you want to write a plugin that works with this library, and you see a function in another BeTo Module that you would like to use, you can always create a ticket to request it to be moved.

## For Game Masters

## For Developers

### Hooks

* `beto.library.loaded` is called with the beto object, containing the base functions.

### Accessible Methods

- beto
    - `onCanvasReady(callback): void` if called after the canvas is already ready the callback will be called directly with the existing canvas
    - `getCanvas(): Promise(canvas)`
    - `calculateDistance(actorToken, placeable): int`
    - `getOnlyInSet(set): mixed` returns the only item in a `Set`
    - `getTokenForActor(actor): Token`
    - `getActivePlayerTokens(canvas): array` returns all controlled tokens or the token belonging to `game.user.character`
    - `findTokenById(canvas, id): Token`
    - `getReach(actor): int` defaults to 5
    - `canInteract(actorToken, placeable): bool`
    - `getModule(moduleName): Promise(module)`
    - `registerModule(moduleName, module): void`

### Register & Get Module

All BeTo Modules are registered in this library. This allows any 3rd party developer to get the module and call functions specified in that module. This uses promises, so the correct way to get a module is: `beto.getModule('adventuring.chests').then(chests => { // Do something with chests });`

The canvas can also be fetched from the BeTo Module with the getCanvas, and the onCanvasReady functions.

### Future plans

- A discord server for bugs, feature requests and questions
- Proper localization instead of hard-coded English
- Compiling the JS files
